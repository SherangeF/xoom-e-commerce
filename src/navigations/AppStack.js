import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchUser } from "../actions/userAction";
import { createDrawerNavigator } from "@react-navigation/drawer";
import ProductStackNavigator from "./ProductStack";
import CartStackNavigator from "./CartStack";
import AuthStackNavigator from "./AuthStack";
import OrderStackNavigator from "./OrderStack";
import DrawerContent from "../components/DrawerContent";

const Drawer = createDrawerNavigator();

const AppStackNavigator = (props) => {
  const token = useSelector((state) => state.user.token);

  //connect redux actions
  const dispatch = useDispatch();

  if (token) {
    dispatch(fetchUser(token));
  }

  return (
    <Drawer.Navigator
      initialRouteName="ProductStack"
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="AuthStack" component={AuthStackNavigator} />
      <Drawer.Screen name="ProductStack" component={ProductStackNavigator} />
      <Drawer.Screen name="CartStack" component={CartStackNavigator} />
      <Drawer.Screen name="OrderStack" component={OrderStackNavigator} />
    </Drawer.Navigator>
  );
};

export default AppStackNavigator;
