import React from "react";
import { connect } from "react-redux";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import BackImage from "../components/Common/BackImage";
import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";
import ForgotPassword from "../screens/ForgotPassword";
import Verification from "../screens/Verification";
import ResetPassword from "../screens/ResetPassword";

const Stack = createStackNavigator();

const AuthStackNavigator = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={({ route }) => ({
          title: "Access Account",
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTitleAlign: "center",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        })}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={({ route }) => ({
          title: "Create Account",
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTitleAlign: "center",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        })}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={({ route }) => ({
          title: "Forgot Password",
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTitleAlign: "center",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        })}
      />
      <Stack.Screen
        name="Verification"
        component={Verification}
        options={({ route }) => ({
          title: "Verification",
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTitleAlign: "center",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        })}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={({ route }) => ({
          title: "Reset Password",
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTitleAlign: "center",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        })}
      />
    </Stack.Navigator>
  );
};

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
});

export default connect(mapStateToProps)(AuthStackNavigator);
