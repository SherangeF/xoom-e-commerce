import React from "react";
import { connect } from "react-redux";
import { TouchableOpacity, Text } from "react-native";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import BackImage from "../components/Common/BackImage";
import ProductList from "../screens/ProductList";
import ProductDetail from "../screens/ProductDetail";
import Cart from "../screens/Cart";
import Checkout from "../screens/Checkout";
import Sucess from "../screens/Sucess";
import { userLogOut } from "../actions/userAction";
import AuthStackNavigator from "./AuthStack";

const Stack = createStackNavigator();

const ProductStackNavigator = (props) => (
  <Stack.Navigator>
    <Stack.Screen
      name="ProductList"
      component={ProductList}
      options={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    />
    <Stack.Screen
      name="ProductDetail"
      component={ProductDetail}
      options={({ route }) => ({
        title: route.params.item.name,
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
    <Stack.Screen
      name="Cart"
      component={Cart}
      options={({ route }) => ({
        title: "Cart Item",
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
    <Stack.Screen
      name="Login"
      component={AuthStackNavigator}
      options={({ route }) => ({
        headerShown: false,
      })}
    />
    <Stack.Screen
      name="Checkout"
      component={Checkout}
      options={({ route }) => ({
        title: "Checkout",
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
    <Stack.Screen
      name="Sucess"
      component={Sucess}
      options={({ route }) => ({
        title: "",
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
      })}
    />
  </Stack.Navigator>
);

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
});

export default connect(mapStateToProps)(ProductStackNavigator);
