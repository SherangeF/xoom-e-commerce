import React from "react";
import { connect } from "react-redux";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import BackImage from "../components/Common/BackImage";
import Cart from "../screens/Cart";
import Checkout from "../screens/Checkout";
import { userLogOut } from "../actions/userAction";

const Stack = createStackNavigator();

const CartStackNavigator = (props) => (
  <Stack.Navigator>
    <Stack.Screen
      name="Cart"
      component={Cart}
      options={({ route }) => ({
        title: "Cart Item",
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
    <Stack.Screen
      name="Checkout"
      component={Checkout}
      options={({ route }) => ({
        title: "Checkout",
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
  </Stack.Navigator>
);

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
});

const mapDispatchToProps = (dispatch) => ({
  userLogOut: (userData) => dispatch(userLogOut(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartStackNavigator);
