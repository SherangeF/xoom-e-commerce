import React from "react";
import { connect } from "react-redux";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import BackImage from "../components/Common/BackImage";
import OrderList from "../screens/OrderList";
import OrderView from "../screens/OrderView";

const Stack = createStackNavigator();

const OrderStackNavigator = (props) => (
  <Stack.Navigator>
    <Stack.Screen
      name="OrderList"
      component={OrderList}
      options={({ route }) => ({
        title: "Your Orders",
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerBackTitle: "",
        headerTruncatedBackTitle: "",
        headerBackImage: () => <BackImage />,
      })}
    />
    <Stack.Screen
      name="OrderView"
      component={OrderView}
      options={({ route }) => {
        return {
          title: route.params.number,
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerBackTitle: "",
          headerTruncatedBackTitle: "",
          headerBackImage: () => <BackImage />,
        };
      }}
    />
  </Stack.Navigator>
);

export default OrderStackNavigator;
