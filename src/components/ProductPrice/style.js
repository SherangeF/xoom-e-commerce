import React from "react";
import {
  THEME_BLACK_COLOR,
  THEME_WHITE_COLOR,
  THEME_BISMARK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  priceWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  unitContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 16,
    marginTop: 16,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "#DAE3EC",
  },
  unitText: {
    fontSize: 13,
    color: THEME_BISMARK_COLOR,
    marginHorizontal: 8,
  },
  quantityText: {
    fontSize: 13,
    color: THEME_BLACK_COLOR,
    fontWeight: "800",
    marginHorizontal: 8,
  },
  priceContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    marginTop: 16,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "#DAE3EC",
  },
  priceText: {
    paddingHorizontal: 16,
  },
  plusButton: {
    paddingHorizontal: 16,
    borderRightWidth: 1,
    borderColor: "#DAE3EC",
  },
  minusButton: {
    paddingHorizontal: 16,
    borderLeftWidth: 1,
    borderColor: "#DAE3EC",
  },
});

export default styles;
