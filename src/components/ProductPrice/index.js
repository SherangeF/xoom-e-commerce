import React from "react";
import { TouchableOpacity, View, Text } from "react-native";
import { Entypo } from "@expo/vector-icons";
import styles from "./style";

export default ProductPrice = (props) => {
  return (
    <View style={styles.priceWrapper}>
      <View style={styles.unitContainer}>
        <Text style={styles.unitText}>UNIT</Text>
        <Text style={styles.quantityText}>KILOS</Text>
      </View>

      <View style={styles.priceContainer}>
        <TouchableOpacity
          style={styles.plusButton}
          onPress={() => props.addItem()}
        >
          <Entypo name="plus" size={12} color="black" />
        </TouchableOpacity>

        <View style={styles.priceText}>
          <Text>{props.itemCount}</Text>
        </View>

        <TouchableOpacity
          style={styles.minusButton}
          onPress={() => props.removeItem()}
        >
          <Entypo name="minus" size={12} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
};
