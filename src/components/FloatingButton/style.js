import React from "react";
import { THEME_WHITE_COLOR, THEME_BLUE_COLOR } from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: "row",
    backgroundColor: THEME_BLUE_COLOR,
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderRadius: 3,

    marginHorizontal: 8,
    marginBottom: 8,

    // bottom: 10,
    // right: 16,
    // left: 16,
    // position: "absolute",
    // zIndex: 100,
  },
  textContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  textStyle: {
    color: THEME_WHITE_COLOR,
    fontSize: 16,
  },
  priceContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
  },
  priceText: {
    color: THEME_WHITE_COLOR,
    fontSize: 18,
    fontWeight: "800",
  },
});

export default styles;
