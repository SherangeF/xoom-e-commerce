import React from "react";
import styles from "./style";
import { View, Text, TouchableOpacity } from "react-native";

export default FloatingButton = (props) => {
  return (
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={() => props.onPress()}
    >
      <View style={styles.textContainer}>
        <Text style={styles.textStyle}>{props.lable}</Text>
      </View>
      <View style={styles.priceContainer}>
        <Text style={styles.priceText}>
          Rs. {props.value ? props.value.toFixed(2) : "0.00"}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
