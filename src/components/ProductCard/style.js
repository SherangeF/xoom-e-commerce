import React from "react";
import {
  THEME_BLACK_COLOR,
  THEME_WHITE_COLOR,
  THEME_RED_COLOR,
  THEME_BISMARK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: THEME_WHITE_COLOR,
    margin: 8,
  },
  cardThumbnail: {
    width: "100%",
    height: 160,
    borderRadius: 6,
  },
  onSaleContainer: {
    position: "absolute",
    top: 10,
    left: 10,
    backgroundColor: THEME_RED_COLOR,
    borderRadius: 2,
    height: 17,
    width: 55,
    justifyContent: "center",
  },
  onSaleText: {
    color: THEME_WHITE_COLOR,
    fontSize: 10,
    textAlign: "center",
    fontWeight: "800",
  },
  productTitle: {
    fontSize: 14,
    color: THEME_BLACK_COLOR,
    marginTop: 10,
  },
  productQuntity: {
    fontSize: 10,
    color: THEME_BISMARK_COLOR,
    marginTop: 2,
  },
  productPrice: {
    fontSize: 14,
    color: THEME_BLACK_COLOR,
    marginTop: 15,
  },
  price: {
    fontWeight: "800",
  },
  cutOffPrice: {
    fontSize: 12,
    color: THEME_BISMARK_COLOR,
    marginTop: 2,
    textDecorationLine: "line-through",
  },
});

export default styles;
