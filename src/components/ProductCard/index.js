import React from "react";
import styles from "./style";
import { TouchableOpacity, View, Text, Image } from "react-native";

export default ProductCard = ({ item, navigation, fetchProduct }) => {
  return (
    <TouchableOpacity
      style={styles.cardContainer}
      onPress={() => {
        fetchProduct(item.id);
        navigation.navigate("ProductDetail", { item });
      }}
    >
      <Image
        source={{
          uri:
            item.item_attachments.length > 0
              ? item.item_attachments[0].attachment_link
              : "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-3-300x225.jpg",
        }}
        style={styles.cardThumbnail}
      />

      {item.available_quantity && (
        <View style={styles.onSaleContainer}>
          <Text style={styles.onSaleText}>ON SALE</Text>
        </View>
      )}

      <Text style={styles.productTitle}>{item.name}</Text>

      <Text style={styles.productQuntity}>Approx. 4 -6 tubes</Text>

      <Text style={styles.productPrice}>
        {"Rs "}
        <Text style={styles.price}>{item.item_details[0].purchase_price}</Text>
        <Text style={{ fontSize: 10, color: "gray" }}> / {item.unit.name}</Text>
      </Text>

      {item.item_details[0].selling_price && (
        <Text style={styles.cutOffPrice}>
          Rs. {item.item_details[0].selling_price}
        </Text>
      )}
    </TouchableOpacity>
  );
};
