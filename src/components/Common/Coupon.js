import React from "react";
import { View, TextInput, Text, TouchableOpacity } from "react-native";
export default Coupon = (props) => {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 8,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: "#A5B9CB",
      }}
    >
      <TextInput
        style={{
          flex: 4,
          height: 45,
          padding: 10,
        }}
        autoCapitalize={"none"}
        returnKeyType={"done"}
        returnKeyLabel={"Done"}
        secureTextEntry={props.secureTextEntry ? true : false}
        placeholderTextColor={"#A5B9CB"}
        value={props.value}
        onChangeText={props.onChangeText}
        placeholder={props.placeholder}
      />
      <TouchableOpacity
        style={{
          flex: 1,
          justifyContent: "center",
          paddingHorizontal: 10,
        }}
      >
        <Text style={{ color: "#DAE3EC", fontSize: 13, fontWeight: "800" }}>
          APPLY
        </Text>
      </TouchableOpacity>
    </View>
  );
};
