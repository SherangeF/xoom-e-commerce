import React from "react";
import Modal from "react-native-modal";
import { Ionicons } from "@expo/vector-icons";
import { View, Text, FlatList, TouchableOpacity } from "react-native";

export default ModalPicker = (props) => {
  return (
    <View style={{ marginVertical: 10 }}>
      <Text style={{ fontSize: 14 }}>{props.label}</Text>
      <TouchableOpacity
        style={{
          height: 56,
          borderColor: "#DAE3EC",
          borderWidth: 1,
          borderRadius: 8,
          marginTop: 20,
          padding: 10,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
        onPress={props.toogleModal}
      >
        {props.value ? (
          <Text>{props.value}</Text>
        ) : (
          <Text style={{ color: "#A5B9CB" }}>{props.label}</Text>
        )}
        <Ionicons
          name="ios-arrow-down"
          style={{ paddingHorizontal: 16 }}
          size={24}
          color={"#A5B9CB"}
        />
      </TouchableOpacity>
      <Modal
        isVisible={props.isVisible}
        style={{ justifyContent: "flex-end", margin: 8 }}
        onSwipeComplete={props.toogleModal}
        swipeDirection={["up", "left", "right", "down"]}
      >
        <View style={{ backgroundColor: "#FFFFFF", borderRadius: 4 }}>
          <View
            style={{
              flexDirection: "row",
              paddingHorizontal: 16,
              paddingVertical: 16,
              borderBottomWidth: 1,
              borderBottomColor: "#DAE3EC",
              justifyContent: "space-between",
              marginBottom: 16,
            }}
          >
            <TouchableOpacity>
              <Text
                style={{ color: "#000000", fontSize: 16, fontWeight: "600" }}
              >
                Select {props.label}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={props.toogleModal}>
              <Text style={{ color: "#215EFD", fontSize: 16 }}>Cancel</Text>
            </TouchableOpacity>
          </View>

          <FlatList
            data={props.dataList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  style={{ paddingHorizontal: 16, paddingVertical: 16 }}
                  onPress={() => props.onSelectOptions(item, props.label)}
                >
                  <Text style={{ color: "#000000", fontSize: 16 }}>{item}</Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </Modal>
    </View>
  );
};
