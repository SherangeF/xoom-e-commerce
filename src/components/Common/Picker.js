import React from "react";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import DropDownPicker from "react-native-dropdown-picker";

export default Picker = (props) => {
  return (
    <DropDownPicker
      items={props.items}
      zIndex={props.zIndex}
      defaultValue={props.selectedItem}
      containerStyle={{ height: 40, marginTop: 20 }}
      style={{ backgroundColor: THEME_WHITE_COLOR }}
      itemStyle={{
        justifyContent: "flex-start",
      }}
      dropDownStyle={{ backgroundColor: THEME_WHITE_COLOR }}
      onChangeItem={(item) => props.onChangeItem(item)}
    />
  );
};
