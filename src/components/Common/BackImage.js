import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { THEME_BLACK_COLOR } from "../../constants/theme";

export default BackImage = () => (
  <Ionicons
    name="ios-arrow-back"
    style={{ paddingHorizontal: 16 }}
    size={24}
    color={THEME_BLACK_COLOR}
  />
);
