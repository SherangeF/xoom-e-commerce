import React from "react";
import moment from "moment";
import { View, Text, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { THEME_WHITE_COLOR } from "../../constants/theme";

const OrderItem = ({ item, handleNavigate }) => {
  return (
    <TouchableOpacity
      onPress={() => handleNavigate(item.id, item.number)}
      style={{
        paddingHorizontal: 18,
        paddingVertical: 8,
        marginVertical: 8,
        borderBottomColor: "#DAE3EC",
        borderBottomWidth: 1,
      }}
    >
      <View
        style={{
          flexDirection: "row",
          marginBottom: 8,
        }}
      >
        <Text style={{ color: "#000000", fontSize: 18, fontWeight: "600" }}>
          {item.number}
        </Text>
      </View>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: 8,
        }}
      >
        <Text style={{ color: "#526E88", fontSize: 14 }}>
          Placed on{" "}
          {moment(item.date, "YYYY-MM-DD HH:mm:ss").format("MMM DD YYYY")}
        </Text>
        <Ionicons name="ios-arrow-forward" size={24} color="black" />
      </View>

      <View
        style={{
          flexDirection: "row",
          marginBottom: 8,
        }}
      >
        <Text
          style={{
            color: "#215EFD",
            fontSize: 12,
            fontWeight: "600",
            paddingHorizontal: 10,
            paddingVertical: 3,
            borderWidth: 1,
            borderRadius: 2,
            borderColor: "#215EFD",
          }}
        >
          {item.sales_order_status}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default OrderItem;
