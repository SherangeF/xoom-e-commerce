import React from "react";
import {
  THEME_BLACK_COLOR,
  THEME_WHITE_COLOR,
  THEME_BISMARK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: "row",
    backgroundColor: THEME_BLACK_COLOR,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  drawerIconContainer: {
    flex: 1,
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    borderRightColor: THEME_BISMARK_COLOR,
    borderRightWidth: 1,
    padding: 10,
    backgroundColor: THEME_WHITE_COLOR,
    justifyContent: "center",
    alignItems: "center",
  },
  inputContainer: {
    flex: 8,
    justifyContent: "center",
    backgroundColor: THEME_WHITE_COLOR,
    padding: 10,
  },
  inputStyle: {
    height: 30,
  },
  searchIconContainer: {
    flex: 1,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderRightColor: THEME_BISMARK_COLOR,
    borderRightWidth: 1,
    padding: 10,
    backgroundColor: THEME_WHITE_COLOR,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default styles;
