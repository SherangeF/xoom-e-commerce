import React from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";
import { Entypo } from "@expo/vector-icons";
import styles from "./style";

export default CartItem = (props) => {
  const { item, itemCount } = props;
  return (
    <View style={styles.itemContainer}>
      <Image
        style={styles.imageStyle}
        source={{
          uri:
            item.item_attachments.length > 0
              ? item.item_attachments[0].attachment_link
              : "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-3-300x225.jpg",
        }}
      />
      <View style={styles.detailContainer}>
        <View style={styles.textRow}>
          <Text style={styles.ttileText}>{item.name}</Text>
        </View>

        <View style={styles.textRow}>
          <Text style={styles.subTitleText}>{item.description}</Text>
        </View>

        <View style={styles.priceTextRaw}>
          <Text style={styles.priceText}>
            Rs {item.item_details[0].purchase_price}
          </Text>
          <View style={styles.priceContainer}>
            <TouchableOpacity
              style={styles.plusButton}
              onPress={() => props.addItem(item)}
            >
              <Entypo name="plus" size={12} color="#215EFD" />
            </TouchableOpacity>

            <View style={styles.qtyText}>
              <Text>{itemCount}</Text>
            </View>

            <TouchableOpacity
              style={styles.minusButton}
              onPress={() => props.removeItem(item)}
            >
              <Entypo name="minus" size={12} color="#215EFD" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};
