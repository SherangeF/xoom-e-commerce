import {
  THEME_WHITE_COLOR,
  THEME_LIGHT_BLUE,
  THEME_BLUE_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    marginVertical: 8,
  },
  imageStyle: {
    width: 90,
    height: 90,
    borderRadius: 8,
  },
  detailContainer: {
    flex: 1,
    marginLeft: 16,
  },
  textRow: {
    flexDirection: "row",
  },
  ttileText: {
    fontSize: 16,
    fontWeight: "600",
  },
  subTitleText: {
    fontSize: 14,
    marginVertical: 6,
    color: "#526E88",
  },
  priceTextRaw: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  priceText: {
    fontSize: 16,
    fontWeight: "600",
  },
  priceContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    // marginTop: 16,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: "#DAE3EC",
  },
  qtyText: {
    paddingHorizontal: 16,
  },
  plusButton: {
    paddingHorizontal: 16,
    borderRightWidth: 1,
    borderColor: "#DAE3EC",
  },
  minusButton: {
    paddingHorizontal: 16,
    borderLeftWidth: 1,
    borderColor: "#DAE3EC",
  },
});

export default styles;
