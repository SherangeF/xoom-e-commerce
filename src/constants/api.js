export const API_URL = "http://erp.api.dev.xoomsoftware.com/api/";

export const API_VERSION = "v1";

export const ORGANIZATION_ID = "4";

export const API_END_POINTS = {
  SIGNUP: `commerce/${API_VERSION}/auth/register`,
  SIGNIN: `commerce/${API_VERSION}/auth/login`,
  AUTH_USER: `commerce/${API_VERSION}/contact`,
  FORGOT_PASSWORD: `commerce/${API_VERSION}/auth/forgot-password`,
  RESET_PASSWORD: `commerce/${API_VERSION}/auth/reset-password`,
  PRODUCT_LIST: `commerce/${API_VERSION}/items/${ORGANIZATION_ID}`,
  PRODUCT_DETAIL: `commerce/${API_VERSION}/item/`,
  CHECKOUT: `commerce/${API_VERSION}/sales-order`,
  ORDER_LIST: `commerce/${API_VERSION}/sales-orders`,
  GET_ORDER: `commerce/${API_VERSION}/sales-order/`,
  GET_SHIPING_METHOD: `commerce/${API_VERSION}/shipping-methods`,
};
