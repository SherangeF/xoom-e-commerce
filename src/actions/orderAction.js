import * as Types from "./actionTypes";
import { API_URL, API_END_POINTS } from "../constants/api";
import { showMessage } from "react-native-flash-message";
import axios from "axios";

export const fetchOrder = (id, token) => async (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.GET_ORDER + id;
  axios
    .get(apiEndPoint, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `${token}`,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        dispatch({ type: Types.ORDER_DETAIL, payload: response.data.data });
      }
    })
    .catch((error) => {
      console.log("error", error.response);
    });
};
