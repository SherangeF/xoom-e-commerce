import * as Types from "./actionTypes";
import { API_URL, API_END_POINTS, ORGANIZATION_ID } from "../constants/api";
import { showMessage } from "react-native-flash-message";
import axios from "axios";

export const userSignUp = (userData) => (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.SIGNUP;
  axios
    .post(apiEndPoint, userData, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => {
      dispatch({ type: Types.FETCHED });
      if (response && response.status === 201) {
        showMessage({
          icon: "auto",
          message: response.data.message,
          type: "success",
          duration: 2000,
        });
        dispatch(
          userSignIn({
            username: userData.email,
            password: userData.password,
            organization_id: ORGANIZATION_ID,
          })
        );
      }
    })
    .catch((error) => {
      dispatch({ type: Types.FETCHED });
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.message,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const userSignIn = (userData) => (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.SIGNIN;
  axios
    .post(apiEndPoint, userData, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => {
      dispatch({ type: Types.FETCHED });
      if (response && response.status === 200) {
        dispatch({
          type: Types.AUTH_USER_TOKEN,
          payload: `Bearer ${response.data.access_token}`,
        });
        dispatch(fetchUser(`Bearer ${response.data.access_token}`));
      }
    })
    .catch((error) => {
      dispatch({ type: Types.FETCHED });
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.error,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const fetchUser = (token) => async (dispatch) => {
  const apiEndPoint = API_URL + API_END_POINTS.AUTH_USER;
  axios
    .get(apiEndPoint, {
      headers: {
        Authorization: `${token}`,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        dispatch({ type: Types.AUTH_USER, payload: response.data.data });
      }
    })
    .catch((error) => {
      dispatch(userLogOut());
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.error,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const forgotPassword = (data) => async (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.FORGOT_PASSWORD;
  axios
    .post(apiEndPoint, data)
    .then((response) => {
      dispatch({ type: Types.FETCHED });
      if (response && response.status === 200) {
        dispatch({ type: Types.EMAIL_SEND_SUCCESS, payload: true });
        showMessage({
          icon: "auto",
          message: response.data.message,
          type: "success",
          duration: 2000,
        });
      }
    })
    .catch((error) => {
      dispatch({ type: Types.FETCHED });
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.message,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const resetPassword = (data) => async (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.RESET_PASSWORD;
  axios
    .post(apiEndPoint, data)
    .then((response) => {
      console.log("response", response);
      dispatch({ type: Types.FETCHED });
      if (response && response.status === 200) {
        showMessage({
          icon: "auto",
          message: response.data.message,
          type: "success",
          duration: 2000,
        });
      }
    })
    .catch((error) => {
      dispatch({ type: Types.FETCHED });
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.message,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const clearStatus = () => async (dispatch) => {
  dispatch({ type: Types.EMAIL_SEND_SUCCESS, payload: false });
};

export const userLogOut = () => async (dispatch) => {
  dispatch({ type: Types.AUTH_USER, payload: null });
  dispatch({ type: Types.AUTH_USER_TOKEN, payload: null });
  dispatch({ type: Types.FETCHED });
};
