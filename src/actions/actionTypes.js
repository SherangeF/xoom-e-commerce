export const FETCHING = "FETCHING";
export const FETCHED = "FETCHED";
export const AUTH_USER_TOKEN = "AUTH_USER_TOKEN";
export const AUTH_USER = "AUTH_USER";

//Forget password
export const EMAIL_SEND_SUCCESS = "EMAIL_SEND_SUCCESS";

export const PRODUCT_DETAIL = "PRODUCT_DETAIL";

export const ORDER_DETAIL = "ORDER_DETAIL";

export const CART_LIST = "CART_LIST";
export const UPDATE_CART_LIST = "UPDATE_CART_LIST";
export const CHECKOUT_SUCESS = "CHECKOUT_SUCESS";
export const SHIPPING_METHODS = "SHIPPING_METHODS";
