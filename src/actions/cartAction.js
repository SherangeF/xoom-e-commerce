import * as Types from "./actionTypes";
import { API_URL, API_END_POINTS } from "../constants/api";
import { showMessage } from "react-native-flash-message";
import axios from "axios";

export const addToCart = (data) => async (dispatch) => {
  dispatch({ type: Types.CART_LIST, payload: data });
};

export const upDateCart = (data) => async (dispatch) => {
  dispatch({ type: Types.UPDATE_CART_LIST, payload: data });
};

export const checkout = (data, token) => async (dispatch) => {
  const apiEndPoint = API_URL + API_END_POINTS.CHECKOUT;
  axios
    .post(apiEndPoint, data, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `${token}`,
      },
    })
    .then((response) => {
      console.log("response", response);
      dispatch({ type: Types.CHECKOUT_SUCESS, payload: true });
      showMessage({
        icon: "auto",
        message: response.data.message,
        type: "success",
        duration: 2000,
      });
    })
    .catch((error) => {
      console.log("error", error.response);
      if (error && error.response) {
        showMessage({
          icon: "auto",
          message: error.response.data.message,
          type: "danger",
          duration: 2000,
        });
      }
    });
};

export const getShippingMethods = (token) => async (dispatch) => {
  const apiEndPoint = API_URL + API_END_POINTS.GET_SHIPING_METHOD;
  axios
    .get(apiEndPoint, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `${token}`,
      },
    })
    .then((response) => {
      if (response && response.data) {
        let shippingMethods = [];
        response.data.data.map((item) => {
          shippingMethods.push(item.name);
        });
        dispatch({ type: Types.SHIPPING_METHODS, payload: shippingMethods });
      }
    })
    .catch((error) => {
      console.log("error", error.response);
    });
};
export const cartClear = () => async (dispatch) => {
  dispatch({ type: Types.CHECKOUT_SUCESS, payload: false });
  dispatch({ type: Types.UPDATE_CART_LIST, payload: [] });
};
