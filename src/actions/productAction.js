import * as Types from "./actionTypes";
import { API_URL, API_END_POINTS } from "../constants/api";
import { showMessage } from "react-native-flash-message";
import axios from "axios";

export const fetchProduct = (id) => async (dispatch) => {
  dispatch({ type: Types.FETCHING });
  const apiEndPoint = API_URL + API_END_POINTS.PRODUCT_DETAIL + id;
  axios
    .get(apiEndPoint, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => {
      dispatch({ type: Types.FETCHED });
      if (response && response.status === 200) {
        dispatch({ type: Types.PRODUCT_DETAIL, payload: response.data.data });
      }
    })
    .catch((error) => {
      dispatch({ type: Types.FETCHED });
      console.log("error", error.response);
    });
};
