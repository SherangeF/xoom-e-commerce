import * as Types from "../actions/actionTypes";

const initialState = {
  orderDetail: null,
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.ORDER_DETAIL:
      return {
        ...state,
        orderDetail: action.payload,
      };
    default:
      return state;
  }
};

export default orderReducer;
