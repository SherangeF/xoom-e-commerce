import * as Types from "../actions/actionTypes";

const initialState = {
  cartList: [],
  checkoutSucess: false,
  shippingMethods: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.CART_LIST:
      return {
        ...state,
        cartList: [...state.cartList, ...action.payload],
      };
    case Types.UPDATE_CART_LIST:
      return {
        ...state,
        cartList: action.payload,
      };
    case Types.CHECKOUT_SUCESS:
      return {
        ...state,
        checkoutSucess: action.payload,
      };
    case Types.SHIPPING_METHODS:
      return {
        ...state,
        shippingMethods: action.payload,
      };
    default:
      return state;
  }
};

export default cartReducer;
