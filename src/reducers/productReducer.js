import * as Types from "../actions/actionTypes";

const initialState = {
  productDetail: null,
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: action.payload,
      };
    default:
      return state;
  }
};

export default productReducer;
