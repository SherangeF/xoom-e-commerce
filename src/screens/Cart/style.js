import {
  THEME_WHITE_COLOR,
  THEME_LIGHT_BLUE,
  THEME_BLUE_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME_WHITE_COLOR,
  },
  itemContainer: {
    flex: 2,
    paddingHorizontal: 16,
  },
  couponContainer: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  detailContainer: {
    flex: 1,
  },
  labelBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 5,
  },
  labelText: {
    fontSize: 14,
    color: "#526E88",
  },
  amountText: {
    fontSize: 14,
    color: "#000000",
    fontWeight: "600",
  },
  totalLabelText: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "600",
  },
  totalText: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "600",
  },
  removeText: {
    marginLeft: 5,
    color: "#215EFD",
    fontSize: 14,
    fontWeight: "600",
  },
  buttonStyle: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_LIGHT_BLUE,
  },
  buttonStyleActive: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_BLUE_COLOR,
  },
  buttonText: {
    color: THEME_WHITE_COLOR,
    fontSize: 18,
    fontWeight: "800",
  },
  swipeButton: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "red",
    marginLeft: 5,
    marginVertical: 8,
  },
});

export default styles;
