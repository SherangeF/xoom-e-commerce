import React from "react";
import { connect } from "react-redux";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import {
  View,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
} from "react-native";
import Swipeable from "react-native-swipeable-row";
import { AntDesign } from "@expo/vector-icons";
import Coupon from "../../components/Common/Coupon";
import CartItem from "../../components/CartItem";
import { upDateCart } from "../../actions/cartAction";
import styles from "./style";

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      subTotal: 0.0,
      delivery: 0.0,
      discount: 0.0,
      total: 0.0,
      cartList: [],
    };
  }

  componentDidMount() {
    const { cartList } = this.props;
    if (cartList && cartList.length > 0) {
      this.setCartData(cartList);
    }
  }

  componentDidUpdate(prevPorps) {
    const { cartList } = this.props;
    if (cartList !== prevPorps.cartList) {
      this.setState({ subTotal: 0.0, cartList: [] }, () => {
        this.setCartData(cartList);
      });
    }
  }

  setCartData = (cartList) => {
    let { subTotal, total, discount, delivery } = this.state;

    cartList.map((item) => {
      //Item Total
      let itemTotal =
        parseFloat(item.item.item_details[0].purchase_price) * item.itemCount;

      //Sub Total
      subTotal = subTotal + itemTotal;
    });

    //calculate total
    total = subTotal - discount + delivery;

    if (cartList.length > 1) {
      this.props.navigation.setOptions({ title: "Cart Items" });
    } else {
      this.props.navigation.setOptions({ title: "Cart Item" });
    }

    this.setState({ subTotal, cartList, total });
  };

  onChangeText = (text) => {
    this.setState({ value: text });
  };

  handleSubmit = () => {
    const { cartList } = this.props;
    if (cartList.length > 0) {
      this.props.navigation.navigate("Checkout");
    }
  };

  handleRemoveItem = (item) => {
    const { cartList } = this.state;

    let newList = cartList.filter((data) => {
      if (data.item.id !== item.item.id) {
        return data;
      }
    });

    this.props.upDateCart(newList);
  };

  addItem = (item) => {
    const { cartList } = this.state;

    let selectedItem = cartList.find((data) => data.item.id === item.id);
    let selectedIndex = cartList.findIndex((data) => data.item.id === item.id);

    if (selectedItem) {
      selectedItem.itemCount = selectedItem.itemCount + 1;
      cartList.slice(selectedIndex, 1, selectedItem);
      this.setState({ subTotal: 0.0, cartList: [] }, () => {
        this.setCartData(cartList);
      });
    }
  };

  removeItem = (item) => {
    const { cartList } = this.state;

    let selectedItem = cartList.find((data) => data.item.id === item.id);
    let selectedIndex = cartList.findIndex((data) => data.item.id === item.id);

    if (selectedItem && selectedItem.itemCount > 1) {
      selectedItem.itemCount = selectedItem.itemCount - 1;
      cartList.slice(selectedIndex, 1, selectedItem);
      this.setState({ subTotal: 0.0, cartList: [] }, () => {
        this.setCartData(cartList);
      });
    }
  };

  rightButtons = (item) => {
    return [
      <TouchableOpacity
        onPress={() => this.handleRemoveItem(item)}
        style={styles.swipeButton}
      >
        <AntDesign
          name="delete"
          style={{ paddingHorizontal: 24 }}
          size={24}
          color={"#FFFFFF"}
        />
      </TouchableOpacity>,
    ];
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <View style={styles.itemContainer}>
          <FlatList
            data={this.state.cartList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <Swipeable
                onRef={(ref) => (this.swipeable = ref)}
                rightButtons={this.rightButtons(item)}
              >
                <CartItem
                  item={item.item}
                  itemCount={item.itemCount}
                  addItem={this.addItem}
                  removeItem={this.removeItem}
                />
              </Swipeable>
            )}
          />
        </View>

        <View style={styles.couponContainer}>
          <Coupon
            secureTextEntry={false}
            value={this.state.value}
            placeholder={"Enter promo or coupon code"}
            onChangeText={this.onChangeText}
          />

          <View style={styles.detailContainer}>
            <View style={styles.labelBox}>
              <Text style={styles.labelText}>Subtotal</Text>
              <Text style={styles.amountText}>
                Rs. {this.state.subTotal.toFixed(2)}
              </Text>
            </View>

            <View style={styles.labelBox}>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.labelText}>Discount (MOB20)</Text>
                <TouchableOpacity>
                  <Text style={styles.removeText}>Remove</Text>
                </TouchableOpacity>
              </View>

              <Text style={styles.amountText}>
                Rs. {this.state.discount.toFixed(2)}
              </Text>
            </View>

            <View style={styles.labelBox}>
              <Text style={styles.labelText}>Delivery</Text>
              <Text style={styles.amountText}>
                Rs. {this.state.delivery.toFixed(2)}
              </Text>
            </View>

            <View style={styles.labelBox}>
              <Text style={styles.totalLabelText}>Order Total</Text>
              <Text style={styles.totalText}>
                Rs. {this.state.total.toFixed(2)}
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={
            this.props.cartList.length > 0
              ? styles.buttonStyleActive
              : styles.buttonStyle
          }
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Checkout Now</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  productDetail: state.product.productDetail,
  cartList: state.cart.cartList,
});

const mapDispatchToProps = (dispatch) => ({
  upDateCart: (data) => dispatch(upDateCart(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
