import {
  THEME_WHITE_COLOR,
  THEME_BISMARK_COLOR,
  THEME_BLACK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME_WHITE_COLOR,
  },
  detailContainer: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 24,
    borderBottomColor: "#DAE3EC",
    borderBottomWidth: 1,
  },
  orderNumberContainer: {
    flexDirection: "row",
    marginBottom: 4,
  },
  orderNumberText: {
    color: "#000000",
    fontSize: 18,
    fontWeight: "600",
  },
  dateContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 4,
  },
  dateText: {
    color: "#526E88",
    fontSize: 14,
  },
  titleLabel: {
    color: THEME_BISMARK_COLOR,
    fontSize: 13,
    marginBottom: 24,
    fontWeight: "600",
  },
  labelBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 5,
  },
  labelText: {
    fontSize: 14,
    color: "#526E88",
  },
  amountText: {
    fontSize: 14,
    color: "#000000",
    fontWeight: "600",
  },
  totalLabelText: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "600",
  },
  totalText: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "600",
  },
  removeText: {
    marginLeft: 5,
    color: "#215EFD",
    fontSize: 14,
    fontWeight: "600",
  },
  addressContainer: {
    paddingHorizontal: 18,
    paddingVertical: 24,
    borderBottomColor: "#DAE3EC",
    borderBottomWidth: 1,
  },
  addressName: {
    color: THEME_BLACK_COLOR,
    fontSize: 13,
    fontWeight: "600",
    marginBottom: 8,
  },
  addressText: {
    color: THEME_BISMARK_COLOR,
    fontSize: 13,
    marginBottom: 8,
  },
  deliveryContainer: {
    paddingHorizontal: 18,
    paddingVertical: 24,
    borderBottomColor: "#DAE3EC",
    borderBottomWidth: 1,
  },
  deliveryText: {
    color: THEME_BLACK_COLOR,
    fontSize: 13,
    fontWeight: "600",
    marginBottom: 8,
  },
  statusRow: {
    flexDirection: "row",
    marginBottom: 20,
  },
  statusPendingText: {
    color: "#215EFD",
    fontSize: 12,
    fontWeight: "600",
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#215EFD",
  },
  itemContainer: {
    flexDirection: "row",
    marginBottom: 24,
    // marginVertical: 8,
  },
  imageStyle: {
    width: 90,
    height: 90,
    borderRadius: 8,
  },
  itemDetailContainer: {
    flex: 1,
    marginLeft: 16,
  },
  textRow: {
    flexDirection: "row",
  },
  ttileText: {
    fontSize: 16,
    fontWeight: "600",
  },
  subTitleText: {
    fontSize: 14,
    marginVertical: 6,
    color: "#526E88",
  },
  priceTextRaw: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  priceText: {
    fontSize: 16,
    fontWeight: "600",
  },
});

export default styles;
