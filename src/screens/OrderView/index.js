import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import { View, Image, StatusBar, ScrollView, Text } from "react-native";
import { fetchOrder } from "../../actions/orderAction";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import styles from "./style";

class OrderView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderDetail: null,
      value: "",
      subTotal: 0.0,
      delivery: 0.0,
      discount: 0.0,
      total: 0.0,
      isRefresh: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.orderDetail !== prevProps.orderDetail) {
      this.setState({ orderDetail: this.props.orderDetail });
    }
  }

  renderItems = () => {
    const { orderDetail } = this.state;
    if (orderDetail && orderDetail.line_items) {
      return orderDetail.line_items.map((item, index) => {
        return (
          <View style={styles.itemContainer} key={index}>
            <Image
              style={styles.imageStyle}
              source={{
                uri: item.item_attachments
                  ? item.item_attachments[0].attachment_link
                  : "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-3-300x225.jpg",
              }}
            />
            <View style={styles.itemDetailContainer}>
              <View style={styles.textRow}>
                <Text style={styles.ttileText}>{item.name}</Text>
              </View>

              <View style={styles.textRow}>
                <Text style={styles.subTitleText}>{item.description}</Text>
              </View>

              <View style={styles.priceTextRaw}>
                <Text style={styles.priceText}>Rs {item.selling_price}</Text>
              </View>
            </View>
          </View>
        );
      });
    }
  };

  renderStatus = (status) => {
    if (status === "Placed") {
      return (
        <View style={styles.statusRow}>
          <Text style={styles.statusPendingText}>{status}</Text>
        </View>
      );
    }
  };

  render() {
    const { orderDetail } = this.state;
    return (
      <ScrollView style={styles.container}>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <View
          style={{
            // height: 120,
            paddingHorizontal: 18,
            paddingVertical: 24,
            borderBottomColor: "#DAE3EC",
            borderBottomWidth: 1,
          }}
        >
          {orderDetail &&
            orderDetail.sales_order_status &&
            this.renderStatus(orderDetail.sales_order_status)}

          <View style={styles.orderNumberContainer}>
            <Text style={styles.orderNumberText}>
              {orderDetail ? orderDetail.number : ""}
            </Text>
          </View>

          {orderDetail ? (
            <View style={styles.dateContainer}>
              <Text style={styles.dateText}>
                Placed on{" "}
                {moment(orderDetail.date, "YYYY-MM-DD HH:mm:ss").format(
                  "MMM DD YYYY"
                )}
              </Text>
            </View>
          ) : null}
        </View>

        {/* order irems details */}
        <View style={styles.detailContainer}>
          <Text style={styles.titleLabel}>ITEMS ORDERED</Text>
          {this.renderItems()}
          <View style={styles.labelBox}>
            <Text style={styles.labelText}>Subtotal</Text>
            <Text style={styles.amountText}>
              Rs.{" "}
              {orderDetail && orderDetail.subtotal_price
                ? orderDetail.subtotal_price
                : "0.00"}
            </Text>
          </View>

          <View style={styles.labelBox}>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.labelText}>Discount (MOB20)</Text>
            </View>

            <Text style={styles.amountText}>
              Rs.{" "}
              {orderDetail && orderDetail.discount
                ? orderDetail.discount
                : "0.00"}
            </Text>
          </View>

          <View style={styles.labelBox}>
            <Text style={styles.labelText}>Delivery</Text>
            <Text style={styles.amountText}>
              Rs.{" "}
              {orderDetail && orderDetail.delivery
                ? orderDetail.delivery
                : "0.00"}
            </Text>
          </View>

          <View style={styles.labelBox}>
            <Text style={styles.totalLabelText}>Order Total</Text>
            <Text style={styles.totalText}>
              Rs.{" "}
              {orderDetail && orderDetail.total_price
                ? orderDetail.total_price
                : "0.00"}
            </Text>
          </View>
        </View>

        {/* address details */}
        <View style={styles.addressContainer}>
          <Text style={styles.titleLabel}>DELIVERY</Text>
          {/* first_name */}
          <Text style={styles.addressName}>
            {orderDetail && orderDetail.contact
              ? orderDetail.contact.first_name
              : ""}
          </Text>
          {/* street_1 */}
          <Text style={styles.addressText}>
            {orderDetail && orderDetail.shipping_details
              ? orderDetail.shipping_details.street_1
              : ""}
          </Text>
          {/* street_2 */}
          <Text style={styles.addressText}>
            {orderDetail && orderDetail.shipping_details
              ? orderDetail.shipping_details.street_2
              : ""}
          </Text>
          {/* city */}
          <Text style={styles.addressText}>
            {orderDetail && orderDetail.shipping_details
              ? orderDetail.shipping_details.city
              : ""}
          </Text>
          {/* state */}
          <Text style={styles.addressText}>
            {orderDetail && orderDetail.shipping_details
              ? orderDetail.shipping_details.state
              : ""}
          </Text>
          {/* contact_number */}
          <Text style={styles.addressText}>
            {orderDetail && orderDetail.shipping_details
              ? orderDetail.shipping_details.contact_number
              : ""}
          </Text>
        </View>

        <View style={styles.deliveryContainer}>
          <Text style={styles.titleLabel}>PAYMENT</Text>
          <Text style={styles.deliveryText}>Cash on Delivery</Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
  orderDetail: state.order.orderDetail,
});

const mapDispatchToProps = (dispatch) => ({
  fetchOrder: (id, token) => dispatch(fetchOrder(id, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderView);
