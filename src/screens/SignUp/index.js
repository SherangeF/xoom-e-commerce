import React from "react";
import { connect } from "react-redux";
import {
  Text,
  SafeAreaView,
  StatusBar,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TextInputComponent from "../../components/Common/TextInputComponent";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import { userSignUp } from "../../actions/userAction";
import styles from "./style";
import { ORGANIZATION_ID } from "../../constants/api";

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",

      firstName: "",
      lastName: "",
      email: "",
      mobile: "",
      password: "",
    };
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  onChangeFirstName = (text) => {
    this.setState({ firstName: text, error: false });
  };

  onChangeLastName = (text) => {
    this.setState({ lastName: text, error: false });
  };

  onChangeEmail = (text) => {
    this.setState({ email: text, error: false });
  };

  onChangeMobile = (text) => {
    this.setState({ mobile: text, error: false });
  };

  onChangePassword = (text) => {
    this.setState({ password: text, error: false });
  };

  handleSubmit = () => {
    const { firstName, lastName, email, mobile, password } = this.state;
    if (!firstName || !lastName || !email || !mobile || !password) {
      this.setState({ error: true, message: "Fill all required fields" });
    } else if (!this.validateEmail(email)) {
      this.setState({ error: true, message: "Invalid email" });
    } else {
      let userData = {
        contact_type_lookup_id: 1,
        customer_type_lookup_id: 1,
        organization_id: ORGANIZATION_ID,
        first_name: firstName,
        last_name: lastName,
        email: email,
        password: password,
        contact_details: {
          mobile_contact_number: mobile,
        },
      };
      this.props.userSignUp(userData);
    }
  };

  render() {
    const {
      firstName,
      lastName,
      email,
      mobile,
      password,
      error,
      message,
    } = this.state;

    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signUpContainer}>
          <KeyboardAwareScrollView style={styles.scroll}>
            <Text style={styles.titleDescription}>
              One account to manage {"\n"}your orders
            </Text>

            {error && message && <ErrorMessage message={message} />}

            <TextInputComponent
              label={"First Name"}
              value={firstName}
              onChangeText={(text) => this.onChangeFirstName(text)}
              error={error && !firstName}
              style={styles.textInput}
              placeholder={"What is your first name?"}
            />

            <TextInputComponent
              label={"Last Name"}
              onChangeText={(text) => this.onChangeLastName(text)}
              value={lastName}
              error={error && !lastName}
              style={styles.textInput}
              placeholder={"What is your last name?"}
            />

            <TextInputComponent
              label={"Mobile Number"}
              onChangeText={(text) => this.onChangeMobile(text)}
              value={mobile}
              error={error && !mobile}
              style={styles.textInput}
              placeholder={"Mobile number"}
            />

            <TextInputComponent
              label={"Email"}
              onChangeText={(text) => this.onChangeEmail(text)}
              value={email}
              error={error && !email}
              style={styles.textInput}
              placeholder={"Email"}
            />

            <TextInputComponent
              label={"Password"}
              onChangeText={(text) => this.onChangePassword(text)}
              value={password}
              error={error && !password}
              secureTextEntry={true}
              style={styles.textInput}
              placeholder={"Password"}
            />
            <View style={styles.changeSignIn}>
              <Text style={styles.subText}>Have an account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("SignIn")}
              >
                <Text style={styles.loginText}>Sign In</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
        <TouchableOpacity
          style={
            firstName && lastName && email && mobile && password
              ? styles.buttonStyleActive
              : styles.buttonStyle
          }
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Create Now</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  authUser: state.user.authUser,
});

const mapDispatchToProps = (dispatch) => ({
  userSignUp: (userData) => dispatch(userSignUp(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
