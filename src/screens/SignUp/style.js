import React from "react";
import {
  THEME_WHITE_COLOR,
  THEME_LIGHT_BLUE,
  THEME_BLUE_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  signUpContainer: {
    flex: 1,
  },
  scroll: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: THEME_WHITE_COLOR,
  },
  textInput: {
    height: 56,
    borderColor: THEME_LIGHT_BLUE,
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 20,
    padding: 10,
  },
  titleDescription: {
    fontSize: 24,
    fontWeight: "600",
    lineHeight: 37,
    marginVertical: 15,
  },
  changeSignIn: {
    flexDirection: "row",
    marginVertical: 10,
  },
  subText: {
    fontSize: 14,
  },
  loginText: {
    fontSize: 14,
    fontWeight: "600",
    color: THEME_BLUE_COLOR,
  },
  buttonStyle: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_LIGHT_BLUE,
  },
  buttonStyleActive: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_BLUE_COLOR,
  },
  buttonText: {
    color: THEME_WHITE_COLOR,
    fontSize: 18,
    fontWeight: "800",
  },
});

export default styles;
