import React from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import TextInputComponent from "../../components/Common/TextInputComponent";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { userSignIn } from "../../actions/userAction";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import styles from "./style";
import { ORGANIZATION_ID } from "../../constants/api";

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",

      email: "",
      password: "",
    };
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  onChangeEmail = (text) => {
    this.setState({ email: text, error: false });
  };

  onChangePassword = (text) => {
    this.setState({ password: text, error: false });
  };

  handleSubmit = () => {
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: true, message: "Fill all required fields" });
    } else if (!this.validateEmail(email)) {
      this.setState({ error: true, message: "Invalid email" });
    } else {
      let userData = {
        username: email,
        password: password,
        organization_id: ORGANIZATION_ID,
      };
      this.props.userSignIn(userData);
    }
  };

  componentDidUpdate() {
    const { authUser, route, navigation } = this.props;
    if (authUser) {
      if (route.params && route.params.path === "drawer") {
        navigation.navigate("ProductStack");
      } else {
        navigation.pop();
      }
    }
  }

  render() {
    const { email, password, error, message } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signInContainer}>
          <ScrollView style={styles.scroll}>
            <Text style={styles.headerText}>Sign In</Text>

            {error && message && <ErrorMessage message={message} />}

            <TextInputComponent
              label={"Email"}
              onChangeText={(text) => this.onChangeEmail(text)}
              value={email}
              error={error && !email}
              style={styles.textInput}
              placeholder={"Email"}
            />

            <TextInputComponent
              label={"Password"}
              onChangeText={(text) => this.onChangePassword(text)}
              value={password}
              error={error && !password}
              secureTextEntry={true}
              style={styles.textInput}
              placeholder={"Password"}
            />

            <View style={styles.forgetPassword}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("ForgotPassword")}
              >
                <Text style={styles.forgetPasswordText}>Forgot password ?</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.changeSignIn}>
              <Text style={styles.subText}>Don’t have an account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("SignUp")}
              >
                <Text style={styles.loginText}>Create New</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
        <TouchableOpacity
          style={
            email && password ? styles.buttonStyleActive : styles.buttonStyle
          }
          disabled={!email && !password ? true : false}
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Sign In</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  authUser: state.user.authUser,
});

const mapDispatchToProps = (dispatch) => ({
  userSignIn: (userData) => dispatch(userSignIn(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
