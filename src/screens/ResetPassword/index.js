import React from "react";
import { connect } from "react-redux";
import {
  Text,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import TextInputComponent from "../../components/Common/TextInputComponent";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { resetPassword } from "../../actions/userAction";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import styles from "./style";
import { ORGANIZATION_ID } from "../../constants/api";

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",

      password: "",
      confirmPassword: "",
    };
  }

  onChangePassword = (text) => {
    this.setState({ password: text, error: false });
  };

  onChangeConfirmPassword = (text) => {
    this.setState({ confirmPassword: text, error: false });
  };

  handleSubmit = () => {
    const { password, confirmPassword } = this.state;
    const { params } = this.props.route;

    if (!password || !confirmPassword) {
      this.setState({ error: true, message: "Fill all required fileds" });
    } else if (password !== confirmPassword) {
      this.setState({ error: true, message: "Password not match" });
    } else {
      let userData = {
        username: params.email,
        password: password,
        code: params.code,
        email: params.email,
        organization_id: ORGANIZATION_ID,
      };
      this.props.resetPassword(userData);
    }
  };

  render() {
    const { confirmPassword, password, error, message } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signInContainer}>
          <ScrollView style={styles.scroll}>
            <Text style={styles.headerText}>
              Add a new password to {"\n"}secure account
            </Text>

            {error && message && <ErrorMessage message={message} />}

            <TextInputComponent
              label={"Password"}
              onChangeText={(text) => this.onChangePassword(text)}
              value={password}
              error={error && !password}
              secureTextEntry={true}
              style={styles.textInput}
              placeholder={""}
            />

            <TextInputComponent
              label={"Re-Enter Password"}
              onChangeText={(text) => this.onChangeConfirmPassword(text)}
              value={confirmPassword}
              error={error && !confirmPassword}
              secureTextEntry={true}
              style={styles.textInput}
              placeholder={""}
            />
          </ScrollView>
        </SafeAreaView>
        <TouchableOpacity
          style={
            password && confirmPassword
              ? styles.buttonStyleActive
              : styles.buttonStyle
          }
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Update Password</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  authUser: state.user.authUser,
});

const mapDispatchToProps = (dispatch) => ({
  resetPassword: (userData) => dispatch(resetPassword(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
