import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import { fetchOrder } from "../../actions/orderAction";
import { View, StatusBar, FlatList, Text, RefreshControl } from "react-native";
import { THEME_WHITE_COLOR, THEME_BLACK_COLOR } from "../../constants/theme";
import OrderItem from "../../components/OrderItem";
import { API_URL, API_END_POINTS } from "../../constants/api";
import styles from "./style";

class OrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      isRefresh: false,
      isLoading: false,
      pageSize: 10,
      currentPage: 1,
      lastPage: null,
    };
  }

  componentDidMount() {
    this.fetchOrders();
  }

  fetchOrders = () => {
    const { pageSize, searchText, currentPage, dataSource } = this.state;
    const apiEndPoint = API_URL + API_END_POINTS.ORDER_LIST;
    let params = {
      pageSize: pageSize,
      page: currentPage,
    };
    axios
      .get(apiEndPoint, {
        params,
        headers: {
          "Content-Type": "application/json",
          Authorization: `${this.props.token}`,
        },
      })
      .then((response) => {
        if (response && response.status === 200) {
          this.setState({
            dataSource: [...dataSource, ...response.data.data],
            lastPage: response.data.meta.last_page,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log("error", error.response);
      });
  };

  refreshList = () => {
    this.setState({ currentPage: 1, lastPage: null, dataSource: [] }, () => {
      this.fetchOrders();
    });
  };

  loadList = () => {
    if (this.state.lastPage !== this.state.currentPage) {
      this.setState(
        { currentPage: this.state.currentPage + 1, isLoading: true },
        () => {
          this.fetchOrders();
        }
      );
    }
  };

  handleNavigate = (id, number) => {
    const { token, navigation } = this.props;
    this.props.fetchOrder(id, token);
    navigation.navigate("OrderView", { id: id, number: number });
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        {/* order list */}
        <FlatList
          data={this.state.dataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <OrderItem item={item} handleNavigate={this.handleNavigate} />
          )}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefresh}
              tintColor={THEME_BLACK_COLOR}
              onRefresh={() => {
                this.refreshList();
              }}
            />
          }
          onEndReachedThreshold={0.1}
          onEndReached={() => this.loadList()}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
});

const mapDispatchToProps = (dispatch) => ({
  fetchOrder: (id, token) => dispatch(fetchOrder(id, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
