import {
  THEME_WHITE_COLOR,
  THEME_LIGHT_BLUE,
  THEME_BLUE_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME_WHITE_COLOR,
  },
});

export default styles;
