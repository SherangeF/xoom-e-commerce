import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import FloatingButton from "../../components/FloatingButton";
import ProductSearch from "../../components/ProductSearch";
import ProductCard from "../../components/ProductCard";
import { fetchProduct } from "../../actions/productAction";
import { THEME_BLACK_COLOR, THEME_WHITE_COLOR } from "../../constants/theme";
import { API_URL, API_END_POINTS } from "../../constants/api";
import {
  View,
  SafeAreaView,
  StatusBar,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  RefreshControl,
} from "react-native";

class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isRefresh: false,
      searchText: "",
      pageSize: 10,
      dataSource: [],
      currentPage: 1,
      lastPage: null,

      subTotal: 0.0,
    };
  }

  componentDidMount() {
    this.fetchProductList();
    this.getPrice();
  }

  componentDidUpdate(prevProps) {
    const { cartList } = this.props;
    if (cartList !== prevProps.cartList) {
      this.setState({ subTotal: 0.0 }, () => this.getPrice());
    }
  }

  onChangeSearch = (text) => {
    this.setState(
      { searchText: text, currentPage: 1, lastPage: null, dataSource: [] },
      () => {
        this.fetchProductList();
      }
    );
  };

  fetchProductList = () => {
    const apiEndPoint = API_URL + API_END_POINTS.PRODUCT_LIST;
    const { pageSize, searchText, currentPage, dataSource } = this.state;
    let params = {
      pageSize: pageSize,
      search: searchText,
      page: currentPage,
    };
    axios
      .get(apiEndPoint, {
        params,
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response && response.status === 200) {
          this.setState({
            dataSource: [...dataSource, ...response.data.data],
            lastPage: response.data.meta.last_page,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
      });
  };

  refreshList = () => {
    this.setState({ currentPage: 1, lastPage: null, dataSource: [] }, () => {
      this.fetchProductList();
    });
  };

  loadList = () => {
    if (this.state.lastPage !== this.state.currentPage) {
      this.setState(
        { currentPage: this.state.currentPage + 1, isLoading: true },
        () => {
          this.fetchProductList();
        }
      );
    }
  };

  getPrice = () => {
    const { cartList } = this.props;
    let { subTotal } = this.state;
    cartList &&
      cartList.map((item) => {
        let itemTotal =
          parseFloat(item.item.item_details[0].purchase_price) * item.itemCount;

        //Sub Total
        subTotal = subTotal + itemTotal;
      });
    this.setState({ subTotal });
  };

  renderFooter = () => {
    if (!this.state.isLoading) return null;
    return (
      <View style={{ paddingVertical: 20 }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  navigateCart = () => {
    this.props.navigation.navigate("Cart");
  };

  render() {
    const { cartList } = this.props;
    return (
      <React.Fragment>
        <StatusBar backgroundColor={"#000000"} barStyle={"light-content"} />
        <SafeAreaView style={styles.container}>
          <View style={styles.viewStyle}>
            {/* search bar */}
            <ProductSearch
              value={this.state.searchText}
              navigation={this.props.navigation}
              onChangeText={this.onChangeSearch}
            />

            {/* product grid */}
            <FlatList
              data={this.state.dataSource}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              ListFooterComponent={this.renderFooter}
              renderItem={({ item }) => (
                <ProductCard
                  item={item}
                  navigation={this.props.navigation}
                  fetchProduct={this.props.fetchProduct}
                />
              )}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefresh}
                  tintColor={THEME_BLACK_COLOR}
                  onRefresh={() => {
                    this.refreshList();
                  }}
                />
              }
              onEndReachedThreshold={0.1}
              onEndReached={() => this.loadList()}
            />
            {cartList.length > 0 && (
              <FloatingButton
                lable={"View Cart"}
                value={this.state.subTotal}
                onPress={this.navigateCart}
                navigation={this.props.navigation}
              />
            )}
          </View>
        </SafeAreaView>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  token: state.user.token,
  cartList: state.cart.cartList,
});

const mapDispatchToProps = (dispatch) => ({
  fetchProduct: (id) => dispatch(fetchProduct(id)),
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: THEME_BLACK_COLOR,
    justifyContent: "center",
  },
  viewStyle: {
    flex: 1,
    backgroundColor: THEME_WHITE_COLOR,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
