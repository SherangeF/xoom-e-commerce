import React from "react";
import {
  THEME_BLACK_COLOR,
  THEME_WHITE_COLOR,
  THEME_RED_COLOR,
  THEME_BISMARK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    padding: 16,
    backgroundColor: THEME_WHITE_COLOR,
  },
  imageStyle: {
    width: "100%",
    height: 300,
    borderRadius: 6,
  },
  onSaleContainer: {
    position: "absolute",
    top: 10,
    left: 10,
    backgroundColor: THEME_RED_COLOR,
    borderRadius: 2,
    height: 23,
    width: 60,
    justifyContent: "center",
  },
  onSaleText: {
    color: THEME_WHITE_COLOR,
    fontSize: 12,
    textAlign: "center",
    fontWeight: "800",
  },
  productTitle: {
    fontWeight: "800",
    fontSize: 20,
    color: THEME_BLACK_COLOR,
    marginTop: 20,
  },
  productQuantity: {
    fontSize: 14,
    color: THEME_BISMARK_COLOR,
    marginTop: 8,
  },
  productPrice: {
    fontSize: 22,
    color: THEME_BLACK_COLOR,
    marginTop: 15,
  },
  price: {
    fontWeight: "800",
  },
  cutOffPrice: {
    fontSize: 14,
    color: THEME_BISMARK_COLOR,
    marginTop: 8,
    textDecorationLine: "line-through",
  },
  productDescription: {
    fontSize: 14,
    color: THEME_BISMARK_COLOR,
    marginTop: 20,
    marginBottom: 50,
  },
});

export default styles;
