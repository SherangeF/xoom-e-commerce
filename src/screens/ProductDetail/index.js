import React from "react";
import { connect } from "react-redux";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import FloatingButton from "../../components/FloatingButton";
import { View, Image, Text, ScrollView } from "react-native";
import ProductPrice from "../../components/ProductPrice";
import ModalPicker from "../../components/Common/ModalPicker";
import { addToCart } from "../../actions/cartAction";
import styles from "./style";

class ProductDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemDetails: props.route.params.item.item_details[0],
      pickerData: [],
      itemCount: 1,
      error: false,
      message: "",
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {
    if (this.props.productDetail !== prevProps.productDetail) {
      let data = [];
      const { item_details } = this.props.productDetail;

      item_details &&
        item_details.map((item) => {
          item.item_options &&
            item.item_options.value.map((option) => {
              let include = data.find(
                (item) => item.key === option.item_variant_value
              );

              if (!include) {
                data.push({
                  key: option.item_variant_value,
                  values: [option.value],
                });
              } else {
                let includeValues = include.values.find(
                  (item) => item === option.value
                );

                if (!includeValues) include.values.push(option.value);
              }
            });
        });

      this.setState({ pickerData: data });
    }
  }

  toogleModal = (key) => {
    this.setState({ [`isVisible${key}`]: !this.state[`isVisible${key}`] });
  };

  onChangeItem = (item, key) => {
    this.setState(
      {
        [`isVisible${key}`]: false,
        [`selected${key}`]: item,
      },
      () => {
        this.getProductDetails();
      }
    );
  };

  getProductDetails = () => {
    const { item_details } = this.props.productDetail;
    let selectData = [];

    this.state.pickerData.map((item) => {
      if (this.state[`selected${item.key}`])
        selectData.push(this.state[`selected${item.key}`]);
    });

    let filterItem = item_details.find((item) => {
      let valueArray = [];
      item.item_options.value.map((item) => {
        valueArray.push(item.value);
      });

      if (selectData.length != valueArray.length) return false;
      else {
        // comapring each element of array
        for (var i = 0; i < selectData.length; i++)
          if (selectData[i] != valueArray[i]) return false;
        return true;
      }
    });

    if (filterItem) this.setState({ itemDetails: filterItem });
  };

  addItem = () => {
    this.setState({ itemCount: this.state.itemCount + 1 });
  };

  removeItem = () => {
    if (this.state.itemCount !== 1) {
      this.setState({ itemCount: this.state.itemCount - 1 });
    }
  };

  handleCart = () => {
    const { authUser, cartList } = this.props;
    const { item } = this.props.route.params;
    const { itemCount, pickerData } = this.state;
    let notSlectedItem =
      pickerData &&
      pickerData.find((item) => !this.state[`selected${item.key}`]);

    let selectedItem = cartList.findIndex((data) => data.item.id === item.id);

    if (selectedItem !== -1) {
      cartList.splice(selectedItem, 1);
    }

    if (notSlectedItem) {
      this.setState({ error: true, message: "Select avilable options" }, () => {
        this.scrollView.scrollToEnd({ animated: true });
      });
    } else if (authUser) {
      let cartItem = [
        {
          item,
          itemCount,
        },
      ];
      this.props.addToCart(cartItem);
      this.props.navigation.pop();
    } else {
      this.props.navigation.navigate("Login");
    }
  };

  renderPickers = () => {
    const { pickerData } = this.state;
    return (
      pickerData &&
      pickerData.map((item, index) => {
        return (
          <ModalPicker
            label={item.key}
            key={index}
            value={this.state[`selected${item.key}`]}
            dataList={item.values}
            isVisible={this.state[`isVisible${item.key}`]}
            toogleModal={() => this.toogleModal(item.key)}
            onSelectOptions={this.onChangeItem}
          />
        );
      })
    );
  };

  render() {
    const { item } = this.props.route.params;
    const { itemDetails, error, message } = this.state;

    return (
      <View style={{ flex: 1, backgroundColor: THEME_WHITE_COLOR }}>
        <ScrollView
          style={styles.contentWrapper}
          ref={(ref) => {
            this.scrollView = ref;
          }}
        >
          <Image
            source={{
              uri:
                item.item_attachments.length > 0
                  ? item.item_attachments[0].attachment_link
                  : "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-3-300x225.jpg",
            }}
            style={styles.imageStyle}
          />

          {item.onSale && (
            <View style={styles.onSaleContainer}>
              <Text style={styles.onSaleText}>ON SALE</Text>
            </View>
          )}

          <Text style={styles.productTitle}>{item.name}</Text>

          <Text style={styles.productQuantity}>
            Approx.
            {itemDetails.available_quantity
              ? itemDetails.available_quantity
              : 0}
          </Text>

          <ProductPrice
            itemCount={this.state.itemCount}
            addItem={this.addItem}
            removeItem={this.removeItem}
          />

          <Text style={styles.productPrice}>
            Rs.
            <Text style={styles.price}>{itemDetails.purchase_price}</Text>
            <Text style={{ fontSize: 10, color: "gray" }}>
              / {item.unit.name.toUpperCase()}
            </Text>
          </Text>

          {itemDetails.selling_price && (
            <Text style={styles.cutOffPrice}>
              Rs. {itemDetails.selling_price}
            </Text>
          )}

          {this.renderPickers()}

          {error && message && <ErrorMessage message={message} />}

          <Text style={styles.productDescription}>{item.description}</Text>
        </ScrollView>
        <FloatingButton
          lable={"Add to Cart"}
          value={parseFloat(itemDetails.purchase_price) * this.state.itemCount}
          onPress={this.handleCart}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  productDetail: state.product.productDetail,
  cartList: state.cart.cartList,
});

const mapDispatchToProps = (dispatch) => ({
  addToCart: (data) => dispatch(addToCart(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
