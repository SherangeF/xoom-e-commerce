import React from "react";
import {
  THEME_WHITE_COLOR,
  THEME_BLACK_COLOR,
  THEME_BLUE_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  screenWrapper: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_WHITE_COLOR,
    paddingHorizontal: 20,
  },
  headerText: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: "800",
    color: THEME_BLACK_COLOR,
    marginVertical: 16,
  },
  subText: {
    fontSize: 18,
    fontWeight: "900",
    color: THEME_BLACK_COLOR,
    textAlign: "center",
    marginVertical: 16,
    lineHeight: 28,
  },
  buttonStyle: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_BLUE_COLOR,
  },
  buttonText: {
    color: THEME_WHITE_COLOR,
    fontSize: 18,
    fontWeight: "800",
  },
});

export default styles;
