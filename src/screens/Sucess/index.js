import React from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from "react-native";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import { cartClear } from "../../actions/cartAction";
import styles from "./style";

class Sucess extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",
    };
  }

  componentDidMount() {
    this.props.cartClear();
  }

  handleSubmit = () => {
    this.props.navigation.navigate("ProductList");
  };

  render() {
    const { authUser } = this.props;
    return (
      <View style={styles.screenWrapper}>
        <View style={styles.container}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require("../../../assets/sucess.jpeg")}
          ></Image>
          <Text style={styles.headerText}>Thanks, {authUser.first_name}</Text>
          <Text style={styles.subText}>
            Your order has been submitted successfully and we have sent an email
            to you.
          </Text>
        </View>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Back to Home</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.user.authUser,
  productDetail: state.product.productDetail,
  cartList: state.cart.cartList,
});

const mapDispatchToProps = (dispatch) => ({
  cartClear: () => dispatch(cartClear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sucess);
