import React from "react";
import { connect } from "react-redux";
import {
  Text,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import TextInputComponent from "../../components/Common/TextInputComponent";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { forgotPassword } from "../../actions/userAction";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import styles from "./style";
import { ORGANIZATION_ID } from "../../constants/api";

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",
      email: "",
    };
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  onChangeEmail = (text) => {
    this.setState({ email: text, error: false });
  };

  handleSubmit = () => {
    const { email } = this.state;
    if (!email) {
      this.setState({ error: true, message: "Email is required" });
    } else if (!this.validateEmail(email)) {
      this.setState({ error: true, message: "Invalid email" });
    } else {
      let data = {
        email: email,
        organization_id: ORGANIZATION_ID,
        is_mobile: 1,
      };
      this.props.forgotPassword(data);
    }
  };

  componentDidUpdate() {
    if (this.props.emailSend) {
      this.props.navigation.navigate("Verification", {
        email: this.state.email,
      });
    }
  }

  render() {
    const { email, error, message } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signInContainer}>
          <ScrollView style={styles.scroll}>
            <Text style={styles.headerText}>
              Please enter your email address and we will send you an email with
              instructions to reset your password.
            </Text>

            {error && message && <ErrorMessage message={message} />}

            <TextInputComponent
              label={"Email"}
              onChangeText={(text) => this.onChangeEmail(text)}
              value={email}
              error={error && !email}
              style={styles.textInput}
              placeholder={"Email"}
            />
          </ScrollView>
        </SafeAreaView>
        <TouchableOpacity
          disabled={!email ? true : false}
          style={email ? styles.buttonStyleActive : styles.buttonStyle}
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  emailSend: state.user.emailSend,
});

const mapDispatchToProps = (dispatch) => ({
  forgotPassword: (data) => dispatch(forgotPassword(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
