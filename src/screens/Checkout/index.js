import React from "react";
import { connect } from "react-redux";
import {
  Text,
  SafeAreaView,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
  View,
} from "react-native";
import { CheckBox } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TextInputComponent from "../../components/Common/TextInputComponent";
import ModalPicker from "../../components/Common/ModalPicker";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { THEME_WHITE_COLOR } from "../../constants/theme";
import { checkout, getShippingMethods } from "../../actions/cartAction";
import styles from "./style";
import moment from "moment";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",

      billing: {
        address: "",
        street: "",
        city: "",
        state: "",
        contact: "",
        zipCode: "",
        country: "",
      },

      shipping: {
        address: "",
        street: "",
        city: "",
        state: "",
        contact: "",
        zipCode: "",
        country: "",
      },

      checked: false,

      isVisibleOption: false,
      isVisibleMethod: false,

      selectedOption: "",
      selectedMethod: "",

      deliveryOptions: ["Cash on Delivery", "Card on Delivery"],

      deliveryMethod: [],

      subTotal: 0.0,
      items: [],
    };
  }

  componentDidMount() {
    this.getPrice();
    this.props.getShippingMethods(this.props.token);
  }

  componentDidUpdate() {
    if (this.props.checkoutSucess) {
      this.props.navigation.navigate("Sucess");
    }

    if (this.props.shippingMethods !== this.state.deliveryMethod) {
      this.setState({ deliveryMethod: this.props.shippingMethods });
    }
  }

  toogleOption = () => {
    this.setState({ isVisibleOption: !this.state.isVisibleOption });
  };

  toogleMethod = () => {
    this.setState({ isVisibleMethod: !this.state.isVisibleMethod });
  };

  billingAddress = (text) => {
    const billing = { ...this.state.billing, address: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingStreet = (text) => {
    const billing = { ...this.state.billing, street: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingCity = (text) => {
    const billing = { ...this.state.billing, city: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingState = (text) => {
    const billing = { ...this.state.billing, state: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingContact = (text) => {
    const billing = { ...this.state.billing, contact: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingZip = (text) => {
    const billing = { ...this.state.billing, zipCode: text };
    this.setState({ billing, error: false, message: "" });
  };

  billingCountry = (text) => {
    const billing = { ...this.state.billing, country: text };
    this.setState({ billing, error: false, message: "" });
  };

  //Shipping

  shippingAddress = (text) => {
    const shipping = { ...this.state.shipping, address: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingStreet = (text) => {
    const shipping = { ...this.state.shipping, street: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingCity = (text) => {
    const shipping = { ...this.state.shipping, city: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingState = (text) => {
    const shipping = { ...this.state.shipping, state: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingContact = (text) => {
    const shipping = { ...this.state.shipping, contact: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingZip = (text) => {
    const shipping = { ...this.state.shipping, zipCode: text };
    this.setState({ shipping, error: false, message: "" });
  };

  shippingCountry = (text) => {
    const shipping = { ...this.state.shipping, country: text };
    this.setState({ shipping, error: false, message: "" });
  };

  onChangeMethod = (item) => {
    this.setState({
      selectedMethod: item,
      isVisibleMethod: false,
      error: false,
      message: "",
    });
  };

  onSelectOptions = (item) => {
    this.setState({
      selectedOption: item,
      isVisibleOption: false,
      error: false,
      message: "",
    });
  };

  handleSubmit = () => {
    const { billing, shipping, checked } = this.state;
    if (
      !billing.address ||
      !billing.street ||
      !billing.city ||
      !billing.state ||
      !billing.contact ||
      !billing.zipCode ||
      !billing.country
    ) {
      this.scroll.props.scrollToPosition(0, 0);
      this.setState({ error: true, message: "Fill all required fields" });
    } else if (
      !checked &&
      (!shipping.address ||
        !shipping.street ||
        !shipping.city ||
        !shipping.state ||
        !shipping.contact ||
        !shipping.zipCode ||
        !billing.country)
    ) {
      this.scroll.props.scrollToPosition(0, 0);
      this.setState({
        error: true,
        message: "Shipping address fields required",
      });
    } else if (!this.state.selectedMethod) {
      this.scroll.props.scrollToPosition(0, 0);
      this.setState({ error: true, message: "Delivery Method required" });
    } else if (!this.state.selectedOption) {
      this.scroll.props.scrollToPosition(0, 0);
      this.setState({ error: true, message: "Payment Option required" });
    } else {
      let data = {
        reference: null,
        organization_location_id: null,
        date: moment().format("YYYY-MM-DD HH:mm:ss"),
        subtotal_price: this.state.subTotal,
        total_price: this.state.subTotal,
        items: this.state.items,
        billing_details: {
          city: billing.city,
          country_id: 41,
          fax: billing.contact,
          state: billing.state,
          street_1: billing.address,
          street_2: billing.street,
          contact_number: billing.contact,
          zip_code: billing.zipCode,
        },
        shipping_details: {
          city: checked ? billing.city : shipping.city,
          country_id: 41,
          fax: checked ? billing.contact : shipping.contact,
          state: checked ? billing.state : shipping.state,
          street_1: checked ? billing.address : shipping.address,
          street_2: checked ? billing.street : shipping.street,
          contact_number: checked ? billing.contact : shipping.contact,
          zip_code: checked ? billing.zipCode : shipping.zipCode,
        },
      };
      this.props.checkout(data, this.props.token);
    }
  };

  getPrice = () => {
    const { cartList } = this.props;
    let { subTotal, items } = this.state;
    cartList &&
      cartList.map((item) => {
        //item price
        let itemTotal =
          parseFloat(item.item.item_details[0].purchase_price) * item.itemCount;

        //Sub Total
        subTotal = subTotal + itemTotal;

        //add to state in checkout object format
        items.push({
          item_details_id: item.item.item_details[0].id,
          description: item.item.description,
          dimensions: "",
          ean: null,
          is_returnable: 0,
          isbn: null,
          mpn: null,
          name: item.item.name,
          purchase_price: item.item.item_details[0].purchase_price,
          quantity: item.itemCount,
          selling_price: item.item.item_details[0].selling_price,
          sku: item.item.item_details[0].sku,
          organization_tax_id: "",
          upc: null,
          weight: null,
        });
      });

    this.setState({ subTotal, items });
  };

  render() {
    const { error, message, billing, shipping } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signUpContainer}>
          <KeyboardAwareScrollView
            style={styles.scroll}
            innerRef={(ref) => {
              this.scroll = ref;
            }}
          >
            {error && message && <ErrorMessage message={message} />}

            {/* Billing Details */}
            <Text style={styles.productTitle}>Billing Address</Text>
            <TextInputComponent
              label={"Street One"}
              value={billing.address}
              onChangeText={(text) => this.billingAddress(text)}
              error={error && !billing.address}
              style={styles.textInput}
              placeholder={"Street one"}
            />
            <TextInputComponent
              label={"Street Two"}
              value={billing.street}
              onChangeText={(text) => this.billingStreet(text)}
              error={error && !billing.street}
              style={styles.textInput}
              placeholder={"Street two"}
            />
            <TextInputComponent
              label={"City"}
              value={billing.city}
              onChangeText={(text) => this.billingCity(text)}
              error={error && !billing.city}
              style={styles.textInput}
              placeholder={"City"}
            />
            <TextInputComponent
              label={"State"}
              value={billing.state}
              onChangeText={(text) => this.billingState(text)}
              error={error && !billing.state}
              style={styles.textInput}
              placeholder={"State"}
            />

            <TextInputComponent
              label={"Zip Code"}
              value={billing.zipCode}
              onChangeText={(text) => this.billingZip(text)}
              error={error && !billing.zipCode}
              style={styles.textInput}
              placeholder={"Zip code"}
            />
            <TextInputComponent
              label={"Country"}
              value={billing.country}
              onChangeText={(text) => this.billingCountry(text)}
              error={error && !billing.country}
              style={styles.textInput}
              placeholder={"Country"}
            />
            <TextInputComponent
              label={"Contact Number"}
              value={billing.contact}
              onChangeText={(text) => this.billingContact(text)}
              error={error && !billing.contact}
              style={styles.textInput}
              keyboardType={"numeric"}
              placeholder={"Contact number"}
            />

            <CheckBox
              title="Same as billing address"
              checked={this.state.checked}
              onPress={() => this.setState({ checked: !this.state.checked })}
            />

            {/* Shipping Details */}
            <Text style={styles.productTitle}>Shipping Address</Text>
            {!this.state.checked && (
              <View>
                <TextInputComponent
                  label={"Street One"}
                  value={shipping.address}
                  onChangeText={(text) => this.shippingAddress(text)}
                  error={error && !shipping.address}
                  style={styles.textInput}
                  placeholder={"Street one"}
                />
                <TextInputComponent
                  label={"Street Two"}
                  value={shipping.street}
                  onChangeText={(text) => this.shippingStreet(text)}
                  error={error && !shipping.street}
                  style={styles.textInput}
                  placeholder={"Street two"}
                />
                <TextInputComponent
                  label={"City"}
                  value={shipping.city}
                  onChangeText={(text) => this.shippingCity(text)}
                  error={error && !shipping.city}
                  style={styles.textInput}
                  placeholder={"City"}
                />
                <TextInputComponent
                  label={"State"}
                  value={shipping.state}
                  onChangeText={(text) => this.shippingState(text)}
                  error={error && !shipping.state}
                  style={styles.textInput}
                  placeholder={"State"}
                />

                <TextInputComponent
                  label={"Zip Code"}
                  value={shipping.zipCode}
                  onChangeText={(text) => this.shippingZip(text)}
                  error={error && !shipping.zipCode}
                  style={styles.textInput}
                  placeholder={"Zip code"}
                />
                <TextInputComponent
                  label={"Country"}
                  value={shipping.country}
                  onChangeText={(text) => this.shippingCountry(text)}
                  error={error && !shipping.country}
                  style={styles.textInput}
                  placeholder={"Country"}
                />
                <TextInputComponent
                  label={"Contact Number"}
                  value={shipping.contact}
                  onChangeText={(text) => this.shippingContact(text)}
                  error={error && !shipping.contact}
                  style={styles.textInput}
                  keyboardType={"numeric"}
                  placeholder={"Contact number"}
                />
              </View>
            )}

            <ModalPicker
              label={"Delivery method"}
              value={this.state.selectedMethod}
              dataList={this.state.deliveryMethod}
              isVisible={this.state.isVisibleMethod}
              toogleModal={this.toogleMethod}
              onSelectOptions={this.onChangeMethod}
            />
            <ModalPicker
              label={"Payment option"}
              value={this.state.selectedOption}
              dataList={this.state.deliveryOptions}
              isVisible={this.state.isVisibleOption}
              toogleModal={this.toogleOption}
              onSelectOptions={this.onSelectOptions}
            />
          </KeyboardAwareScrollView>
        </SafeAreaView>
        <TouchableOpacity
          style={styles.buttonStyleActive}
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Place Order</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  authUser: state.user.authUser,
  token: state.user.token,
  cartList: state.cart.cartList,
  checkoutSucess: state.cart.checkoutSucess,
  shippingMethods: state.cart.shippingMethods,
});

const mapDispatchToProps = (dispatch) => ({
  checkout: (userData, token) => dispatch(checkout(userData, token)),
  getShippingMethods: (token) => dispatch(getShippingMethods(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
