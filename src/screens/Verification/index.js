import React from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import CodeInput from "react-native-code-input";
import ErrorMessage from "../../components/Common/ErrorMessage";
import { forgotPassword, clearStatus } from "../../actions/userAction";
import { THEME_WHITE_COLOR, THEME_BLUE_COLOR } from "../../constants/theme";
import { ORGANIZATION_ID } from "../../constants/api";
import styles from "./style";

class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: "",
      code: "",
      email: "",
    };
  }

  componentDidMount() {
    const { params } = this.props.route;
    this.props.clearStatus();
    this.setState({ email: params.email });
  }

  changeCode = (text) => {
    this.setState({ code: text, error: false, message: "" });
  };

  handleResend = () => {
    if (this.state.email) {
      let data = {
        email: this.state.email,
        organization_id: ORGANIZATION_ID,
        is_mobile: 1,
      };
      this.props.forgotPassword(data);
    }
  };

  handleSubmit = () => {
    if (!this.state.code) {
      this.setState({ error: true, message: "OTP code required" });
    } else {
      this.props.navigation.navigate("ResetPassword", {
        code: this.state.code,
        email: this.state.email,
      });
    }
  };

  render() {
    const { email, code, error, message } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          backgroundColor={THEME_WHITE_COLOR}
          barStyle={"dark-content"}
        />
        <SafeAreaView style={styles.signInContainer}>
          <ScrollView style={styles.scroll}>
            <Text style={styles.headerText}>
              We’ve sent an code to {"\n"} your email {email ? email : ""}
            </Text>

            {error && message && <ErrorMessage message={message} />}

            <CodeInput
              ref="codeInputRef2"
              codeLength={6}
              secureTextEntry={false}
              activeColor={THEME_BLUE_COLOR}
              inactiveColor="#DAE3EC"
              autoFocus={false}
              inputPosition="center"
              size={50}
              onFulfill={(code) => this.changeCode(code)}
              containerStyle={{ marginTop: 30 }}
              codeInputStyle={styles.codeStyle}
            />

            <View style={styles.resendCode}>
              <Text style={styles.subText}>Didin’t recive code ? </Text>
              <TouchableOpacity onPress={() => this.handleResend()}>
                <Text style={styles.loginText}>Resend</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
        <TouchableOpacity
          style={code ? styles.buttonStyleActive : styles.buttonStyle}
          onPress={() => this.handleSubmit()}
        >
          {this.props.isFetching && (
            <ActivityIndicator
              color={THEME_WHITE_COLOR}
              style={{ marginHorizontal: 10 }}
            />
          )}
          <Text style={styles.buttonText}>Verify Code</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: state.user.isFetching,
  emailSend: state.user.emailSend,
});

const mapDispatchToProps = (dispatch) => ({
  clearStatus: () => dispatch(clearStatus()),
  forgotPassword: (data) => dispatch(forgotPassword(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
