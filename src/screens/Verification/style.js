import React from "react";
import {
  THEME_WHITE_COLOR,
  THEME_LIGHT_BLUE,
  THEME_BLUE_COLOR,
  THEME_BLACK_COLOR,
  THEME_BISMARK_COLOR,
} from "../../constants/theme";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  signInContainer: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: THEME_WHITE_COLOR,
  },
  scroll: {
    flex: 1,
    // paddingHorizontal: 16,
    backgroundColor: THEME_WHITE_COLOR,
  },
  codeStyle: {
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 18,
    fontWeight: "800",
  },
  headerText: {
    fontSize: 24,
    fontWeight: "600",
    lineHeight: 37,
    marginVertical: 30,
  },
  resendCode: {
    flexDirection: "row",
    marginTop: 30,
    marginVertical: 10,
  },
  subText: {
    fontSize: 14,
    color: THEME_BISMARK_COLOR,
  },
  loginText: {
    fontSize: 14,
    fontWeight: "600",
    color: THEME_BLUE_COLOR,
  },
  buttonStyle: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_LIGHT_BLUE,
  },
  buttonStyleActive: {
    flexDirection: "row",
    height: 56,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: THEME_BLUE_COLOR,
  },
  buttonText: {
    color: THEME_WHITE_COLOR,
    fontSize: 18,
    fontWeight: "800",
  },
});

export default styles;
